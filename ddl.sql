CREATE DATABASE instagram;


CREATE TABLE users
(
    id bigserial PRIMARY key,
    username VARCHAR(255) not null,
    email VARCHAR(255) not null,
    passowrd VARCHAR(255) not null
);

CREATE TABLE posts 
(
    id bigserial PRIMARY key,
    user_id BIGINT not null,
    source_url VARCHAR(255) not null,
    post_content VARCHAR(255) not null
);

CREATE TABLE post_likes
(
    id bigserial PRIMARY key,
    post_id bigint not null,
    user_id bigint not null
);

CREATE TABLE post_comments
(
    id bigserial PRIMARY key,
    post_id BIGINT not null,
    user_id BIGINT not null,
    comment TEXT not null
);
