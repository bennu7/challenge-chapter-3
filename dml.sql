-- DML
-- INSERT
INSERT INTO users (username, email, password) VALUES
('ibnu', 'ibnu@gmail.com', 'password'),
('akbar', 'akbar@gmail.com', 'password'),
('deri', 'deri@gmail.com', 'password'),
('febi', 'febi@gmail.com', 'password'),
('lian', 'lian@gmail.com', 'password');


INSERT INTO posts (user_id, source_url, post_content) VALUES
(1, 'https://www.instagram.com/laluibnu__/1', 'best view on my place'),
(1, 'https://www.instagram.com/laluibnu__/2', 'indah sekali pulau lombok ini'),
(2, 'https://www.instagram.com/laluibnu__/3', 'gili trawangan my best place for healing'),
(3, 'https://www.instagram.com/laluibnu__/4', 'indahnya 3 gili di lombok utara '),
(3, 'https://www.instagram.com/laluibnu__/4', 'indahnya pantai sire'), 
(4, 'https://www.instagram.com/laluibnu__/5', 'indahnya pantai impos'), 
(5, 'https://www.instagram.com/laluibnu__/5', 'indahnya pantai medana');


INSERT INTO post_likes (post_id, user_id) VALUES
(1,2),
(1,1),
(1,3),
(1,4),
(1,5),
(2,5),
(2,4),
(3,3),
(3,2),
(3,1),
(4,1),
(4,2),
(5,4),
(5,3);


INSERT INTO post_comments (post_id, user_id, comment) VALUES
(1, 2, 'wahh bagus banget'),
(1, 3, 'wajib planning ke sini'),
(1, 4, 'emang best view nya lombok dah'),
(1, 5, 'anak muda wajib kesini!'),
(2, 4, 'lombok emang indah sedari jaman dulu bray'),
(3, 1, 'wahh gili trawangan its my dream!'),
(4, 2, 'pariwisata wajib maju nihh'),
(5, 4, 'ada putri duyung gaa?'),
(6, 2, 'impos pos pos posss best!'),
(7, 1, 'medana mana iniii?');


-- UPDATE
UPDATE  post_comments 
SET comment = 'ada private jetski?'
WHERE id = 1;

UPDATE  posts 
SET source_url = 'https://www.instagram.com/laluibnu__/9'
WHERE id = 5;


-- DELETE
DELETE FROM post_comments 
WHERE id = 10;

DELETE FROM posts
WHERE source_url = 'https://www.instagram.com/laluibnu__/5';


-- SELECT 
SELECT * FROM users;
SELECT * FROM posts;
SELECT * FROM post_likes;
SELECT * FROM post_comments;
SELECT * FROM users
WHERE username = 'ibnu';

